# Plots categories

This directory holds dedicated classes for each plot category in this package.
Currently available categories are:

- `TrackParametersPlots`: for plots regarding the basic track parameters (pT, eta, etc.). They are saved in the "Tracks/Parameters" sub-directory of the output HIST file.
- `EfficiencyPlots`: for plots regarding the efficiency with respect to the basic track parameters (pT, eta, etc.). They are saved in the "Tracks/Efficiencies" sub-directory of the output HIST file.
- `OfflineElectronPlots`: for plots regarding the parameters corresponding to the linked offline electron parameters (saved in "Tracks/Parameters") and the corresponding efficiencies (saved in "Tracks/Efficiencies").
