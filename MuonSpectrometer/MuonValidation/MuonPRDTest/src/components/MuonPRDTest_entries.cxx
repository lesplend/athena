/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/
#include "../HitValAlg.h"
#include "../NswOccupancyAlg.h"
#include "../MetaDataAlg.h"

DECLARE_COMPONENT( MuonVal::HitValAlg )
DECLARE_COMPONENT( NswOccupancyAlg)
DECLARE_COMPONENT( MuonVal::MetaDataAlg )