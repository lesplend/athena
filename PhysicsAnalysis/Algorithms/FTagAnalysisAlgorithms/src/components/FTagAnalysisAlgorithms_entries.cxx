// AsgExampleTools_entries.cxx

#include <FTagAnalysisAlgorithms/BTaggingEfficiencyAlg.h>
#include <FTagAnalysisAlgorithms/BTaggingInformationDecoratorAlg.h>
#include <FTagAnalysisAlgorithms/BTaggingScoresAlg.h>
#include <FTagAnalysisAlgorithms/XbbInformationDecoratorAlg.h>
#include <FTagAnalysisAlgorithms/XbbEfficiencyAlg.h>

DECLARE_COMPONENT (CP::BTaggingEfficiencyAlg)
DECLARE_COMPONENT (CP::BTaggingInformationDecoratorAlg)
DECLARE_COMPONENT (CP::BTaggingScoresAlg)
DECLARE_COMPONENT (CP::XbbInformationDecoratorAlg)
DECLARE_COMPONENT (CP::XbbEfficiencyAlg)
