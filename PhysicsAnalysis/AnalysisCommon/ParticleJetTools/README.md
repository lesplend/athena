# The ParticleJetTools package

This package contains tools which work on ParticleJet.

## Introduction

For the moment there are following tools (in development):
<table>
<tr>
    <td><b>AlgTool</b></td>
    <td><b>Description</b></td>
</tr>
<tr>
    <td>JetTrackTruthMatch</td>
    <td>Truth match according to TrackParticleTruthContainer.
    </td>
</tr>
<tr>
    <td>JetConeTruthMatch</td>
    <td>A simple cone match tool.
    </td>
</tr>

</table>

## Comments

Please let me know of any errors, or if anything is unclear.
Andreas.Wildauer@cern.ch
